# Day-5-Code-in-Python

GHW: INIT (2023) — Day 5: Code in Python Challenge — Here's the code your Camp leader wrote to find the common book ids in `books_published_last_two_years.txt` and `all_coding_books.txt` to obtain a list of recent coding books.

## Notice
Most rock-solid developers will prefer running this `.ipynb` file in an Anaconda environment, which is quite easy to setup. To get a version of Anaconda up and running in place of VS Code, you can try the open-source version of Anaconda [here](https://www.anaconda.com/products/distribution)

## Introduction
Before getting started, make sure you have the following set of applications setup on your prefered operating system: 
- Visual Studio Code [Official VS Code Website](https://code.visualstudio.com/)
- Python Environmnent: [Install Python](https://www.python.org/)
- Python Extension: [Python environment on VS Code](https://code.visualstudio.com/docs/languages/python)

## Gettig started
- To get started with this project, run the following commands in `bash`:

    - Setup folder:
        ```
        mkdir ghw-init-2023
        cd ghw-init-2023
        ```
    - Setup environment
        ```
        python3 -m venv pyvenv
        source pyvenv/bin/activate
        ```
    - Get the repository files:
        ```
        git clone https://gitlab.com/ghw-init-2023-davidconoh/day-5-code-in-python.git
        cd day-5-code-in-python
        ```
    - Install dependencies
        ```
        pip install -r requirements.txt --upgrade
        ```
- Setup VS Code:
    - Open the `optimizing_code_common_books.ipynb` file in VS Code.
    - Optionally, enter `code .` in the terminal from the current working directory.
    
- Run application:
    - Click ▶️ `Run All` at the navigation bar above the Notebook.

## Test
This project uses the [Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/) for testing.

## Description
This simple Jupyter Notebook is intented to support learning and experiencing Python programming for data analysis and exploration. We carefully examing various methods from the `numpy` library to ascertain the better alternative for carrying out a particular operation on a given data frame. This analysis helps Data experts produce more efficient models and improve our ability to model and make data-driven decisions.

— Global Hack Week: INIT (2023) — Day 5

## Author
- Chika Onoh [@davidconoh](https://gitlab.com/davidconoh)

## Acknowledgment
This project is dedicated to the Major League Hacking Team for organzing a one-of-a-kind Hackathon to kickstart the [MLH Hacking Season 2023](https://ghw.mlh.io/init). Also, appreciation goes all hackers hosting amazing hackathons this season.

Best!

## License
This project is licensed under the [MIT License](https://gitlab.com/ghw-init-2023-davidconoh/day-5-code-in-python/-/blob/main/LICENSE).